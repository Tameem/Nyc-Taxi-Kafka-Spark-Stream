package com.commonUtil

import java.io.File

import com.typesafe.config.ConfigFactory


/**
  * Created by tameemum on 4/7/18.
  */
object Configuration {

  /**
    * This method loads the config file and set values into ConfigBean
    *
    * @return Returns configuration object
    */
  def load: ConfigBean = {
    System.setProperty("config", "./config/kafka.conf")
    val config = ConfigFactory.parseFile(new File(System.getProperty("config")))
    new ConfigBean(config.getString("hosts"), config.getInt("batchSize"), config.getInt("consumerInterval"), config.getString("topic"))
  }
}
