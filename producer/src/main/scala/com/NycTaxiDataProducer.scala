package com

import com.fasterxml.jackson.databind.ObjectMapper
import java.util.{Properties, Random}

import commonUtil.{Configuration, TaxiRecordBean}
import org.apache.log4j.Logger
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}

import scala.io.Source

/**
  * Created by tameemum on 4/7/18.
  */
object NycTaxiDataProducer {
  private[this] lazy val logger = Logger.getLogger(getClass)
  private val RANDOM = new Random
  private val CONFIG = Configuration.load

  def main(args: Array[String]) {
    var producer: KafkaProducer[String, String] = null
    val props = new Properties
    props.put("bootstrap.servers", CONFIG.hosts)
    props.put("acks", "all")
    props.put("retries", "0")
    props.put("batch.size", CONFIG.getBatchSize.toString)
    props.put("linger.ms", "10000000")
    props.put("buffer.memory", "33554432")
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    producer = new KafkaProducer[String, String](props)

    val sleep = RANDOM.nextInt(50) + 200
    val objectMapper = new ObjectMapper
    var record: TaxiRecordBean = null

    /**
      * Reading lines from test.csv file and creating TaxiRecordBean then sending it to kafka broker
      * */
    val bufferedSource = Source.fromFile("./config/test.csv")
    for (line <- bufferedSource.getLines) {
      val cols = line.split(",").map(_.trim)
      record = new TaxiRecordBean(cols(0),cols(1),cols(2),cols(3),cols(4),cols(5),cols(6),cols(7),cols(8))
      producer.send(new ProducerRecord[String, String](CONFIG.getTopic, record.id, objectMapper.writeValueAsString(record)))
      logger.info(record)
      Thread.sleep(sleep)
    }

    bufferedSource.close()
    producer.flush()
    producer.close()

    logger.info("Producer Stopped ...")

  }

}
