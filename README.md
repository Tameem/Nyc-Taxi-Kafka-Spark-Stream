# Nyc Taxi Kafka Spark Stream


This project is a demo for Kafka and Spark stream integration, where producer reads the Nyc taxi data and sends to the kafka broker. The consumer using spark streaming read from the broker and save the anomaly records in parquet file 
Note: It's a very basic project which shows a simple producer and consumer.
### Points to consider
1- Input is present in static file kept under config folder. I have added Thread.sleep to simulate the streaming

2- "linger.ms" is kept very high to demonstrate batch.size as per the requirement.

3- "batch.size" is configurable.

4- Consumer check for a very basic condition "number of passenger >4" to consider the record for anomaly.

5- Output is stored in "config/job-op/" location.

6- As of now the consumer generated lots of smaller parquet out put file. A separate handler should be create to handle smalled files.

7- Consumer runs every 10 minutes and it's configurable 

### Usage

1 - Start zookeeper and kafka service in background.

```bash
$ bin/zookeeper-server-start.sh config/zookeeper.properties
$ bin/kafka-server-start.sh config/server.properties
```

2 - Run the NycTaxiDataProducer.scala
```bash
$ java -jar producer/target/producer-0.1.jar
```


3 - Run the NycTaxiDataConsumer
```bash
$ java -jar producer/target/producer-0.1.jar
```

