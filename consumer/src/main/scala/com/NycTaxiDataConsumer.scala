package com

import java.io.IOException

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import commonUtil.{Configuration, TaxiRecordBean}
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.log4j.Logger
import org.apache.spark.sql.{SaveMode, SparkSession}
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
import org.apache.spark.streaming.kafka010.KafkaUtils
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferBrokers
import org.apache.spark.streaming.{Seconds, StreamingContext}

object NycTaxiDataConsumer {
  private[this] lazy val logger = Logger.getLogger(getClass)

  private[this] val config = Configuration.load
  val objectMapper = new ObjectMapper()
  objectMapper.registerModule(DefaultScalaModule)

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder
      .appName("spark-kafka-streaming-example")
      .master("local[*]")
      .getOrCreate

    val streaming = new StreamingContext(spark.sparkContext, Seconds(config.consumerInterval))

    val servers = config.hosts

    val params = Map[String, Object](
      "bootstrap.servers" -> servers,
      "key.deserializer" -> classOf[StringDeserializer],
      "value.deserializer" -> classOf[StringDeserializer],
      "auto.offset.reset" -> "latest",
      "group.id" -> "NycTaxiData",
      "enable.auto.commit" -> (false: java.lang.Boolean)
    )

    val topics = Array(config.getTopic)

    // creating kafka direct stream object
    val stream = KafkaUtils.createDirectStream(
      streaming, PreferBrokers, Subscribe[String, String](topics, params))


    stream.foreachRDD(rdd => {
      // convert string to PoJo and generate rows as tuple group
      val anomalyRecords = rdd
        .map(row => jsonToObject(row.value().toString))
        .filter(row => {
          var passengerCount: Int = 0;
          try {
            passengerCount = row.passenger_count.toInt
          } catch {
            case e: NumberFormatException => 0
          }
          passengerCount > 4
        })
      if (anomalyRecords.count() > 0) {
        logger.info("saving the anomaly records")
        spark.createDataFrame(anomalyRecords).write.mode(SaveMode.Append).parquet("./config/job-op/")
      }
    })

    streaming.start()
    streaming.awaitTermination()
  }

  /**
    * Json to TaxiRecordBean function
    *
    * @param text the encoded JSON string
    * @return Returns TaxiRecordBean
    */
  def jsonToObject(text: String): TaxiRecordBean = {
    try {
      objectMapper.readValue[TaxiRecordBean](text, classOf[TaxiRecordBean])
    }
    catch {
      case e: IOException =>
        logger.error(e.getMessage, e)
        null
    }
  }
}