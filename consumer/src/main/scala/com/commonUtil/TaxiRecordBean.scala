package com.commonUtil

import scala.beans.BeanProperty

/**
  * Created by tameemum on 4/7/18.
  *
  * This class holds the record
  */
case class TaxiRecordBean
(@BeanProperty id: String,
   @BeanProperty vendor_id: String,
   @BeanProperty pickup_datetime: String,
   @BeanProperty passenger_count: String,
   @BeanProperty pickup_longitude: String,
   @BeanProperty pickup_latitude: String,
   @BeanProperty dropoff_longitude: String,
   @BeanProperty dropoff_latitude: String,
   @BeanProperty store_and_fwd_flag: String
  ) extends Serializable{

  }
