package com.commonUtil

import scala.beans.BeanProperty

/**
  * Created by tameemum on 4/7/18.
  *
  * This class holds the kafka config
  */
case class ConfigBean
(@BeanProperty hosts: String,
 @BeanProperty batchSize: Int,
 @BeanProperty consumerInterval: Int,
 @BeanProperty topic: String
) {}
